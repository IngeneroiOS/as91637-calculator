//
//  CALCFunctionWrapper.m
//  Calculator
//
//  Created by Thomas Roughton on 3/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import "CALCFunction.h"
#import "CALCEquationEvaluator.h"

@implementation CALCFunction

+(CALCFunction*) functionWithType:(CALCFunctionType)type equation:(NSArray *)equation { //Returns a function of the specified type, with the associated sub-equation (can be nil).
    switch (type) {
        case CALCFunctionInverseSign:
            return [self inverseSignFunctionWithEquation:equation];
            break;
        case CALCFunctionLog:
            return [self logFunctionWithEquation:equation];
            break;
        case CALCFunctionSine:
            return [self sineFunctionWithEquation:equation];
            break;
        case CALCFunctionCosine:
            return [self cosineFunctionWithEquation:equation];
            break;
        case CALCFunctionTangent:
            return [self tangentFunctionWithEquation:equation];
            break;
        default:
            return nil;
            break;
    }
}

+(CALCFunction *)logFunctionWithEquation:(NSArray *)equation { //Returns a log function object.
    CALCFunction *logFunction = [[CALCFunction alloc] init];
    logFunction.stringValue = @"log(";
    logFunction.buttonString = @"log";
    logFunction.evaluateFunction = ^ NSDecimalNumber* (NSArray* rhs){
        NSDecimalNumber *rhsValue = [[CALCEquationEvaluator sharedEquationEvaluator] evaluateEquation:rhs];
    
        return [NSDecimalNumber decimalNumberWithDecimal:@(log10(rhsValue.doubleValue)).decimalValue];
    };
    logFunction.showBrackets = YES;
    logFunction.subEquation = [equation mutableCopy];
    return logFunction;
}

+(CALCFunction *)inverseSignFunctionWithEquation:(NSArray *)equation { //Returns a *-1 function object.
    CALCFunction *inverseSignFunction = [[CALCFunction alloc] init];
    inverseSignFunction.buttonString = @"±";
    inverseSignFunction.stringValue = @"-";
    inverseSignFunction.evaluateFunction = ^ NSDecimalNumber* (NSArray* rhs){
        NSDecimalNumber *rhsValue = [[CALCEquationEvaluator sharedEquationEvaluator] evaluateEquation:rhs];
        return [rhsValue decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithMantissa:1 exponent:0 isNegative:YES]];
    };
    inverseSignFunction.showBrackets = NO;
    inverseSignFunction.subEquation = [equation mutableCopy];
    return inverseSignFunction;
}

+(CALCFunction *)sineFunctionWithEquation:(NSArray *)equation { //Returns a sine function object
    CALCFunction *inverseSignFunction = [[CALCFunction alloc] init];
    inverseSignFunction.buttonString = @"sin";
    inverseSignFunction.stringValue = @"sin(";
    inverseSignFunction.evaluateFunction = ^ NSDecimalNumber* (NSArray* rhs){
        NSDecimalNumber *rhsValue = [[CALCEquationEvaluator sharedEquationEvaluator] evaluateEquation:rhs];
        return [NSDecimalNumber decimalNumberWithDecimal:@(sin(rhsValue.doubleValue)).decimalValue];
    };
    inverseSignFunction.showBrackets = YES;
    inverseSignFunction.subEquation = [equation mutableCopy];
    return inverseSignFunction;
}

+(CALCFunction *) cosineFunctionWithEquation:(NSArray *)equation { //Returns a cosine function object
    CALCFunction *inverseSignFunction = [[CALCFunction alloc] init];
    inverseSignFunction.buttonString = @"cos";
    inverseSignFunction.stringValue = @"cos(";
    inverseSignFunction.evaluateFunction = ^ NSDecimalNumber* (NSArray* rhs){
        NSDecimalNumber *rhsValue = [[CALCEquationEvaluator sharedEquationEvaluator] evaluateEquation:rhs];
        
        return [NSDecimalNumber decimalNumberWithDecimal:@(cos(rhsValue.doubleValue)).decimalValue];
    };
    inverseSignFunction.showBrackets = YES;
    inverseSignFunction.subEquation = [equation mutableCopy];
    return inverseSignFunction;
}

+(CALCFunction *) tangentFunctionWithEquation:(NSArray *)equation { //Returns a tangent function object.
    CALCFunction *inverseSignFunction = [[CALCFunction alloc] init];
    inverseSignFunction.buttonString = @"tan";
    inverseSignFunction.stringValue = @"tan(";
    inverseSignFunction.evaluateFunction = ^ NSDecimalNumber* (NSArray* rhs){
        NSDecimalNumber *rhsValue = [[CALCEquationEvaluator sharedEquationEvaluator] evaluateEquation:rhs];
        return [NSDecimalNumber decimalNumberWithDecimal:@(tan(rhsValue.doubleValue)).decimalValue];
    };
    inverseSignFunction.showBrackets = YES;
    inverseSignFunction.subEquation = [equation mutableCopy];
    return inverseSignFunction;
}

-(NSDecimalNumber*) decimalNumberValue {
    return self.evaluateFunction(self.subEquation);
}

@end
