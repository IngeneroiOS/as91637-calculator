//
//  CALCEquationHolder.h
//  Calculator
//
//  Created by Thomas Roughton on 5/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import <Foundation/Foundation.h>

/* The model object for the CALCViewController. Keeps track of the current equation and handles button presses. */

@class CALCButton;

@interface CALCEquationHolder : NSObject

-(void) buttonPressed:(CALCButton*)sender;

@property (nonatomic, strong) NSMutableArray *equation;
@property (nonatomic, strong) NSMutableString *equationString;

@end
