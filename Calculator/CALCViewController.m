//
//  CALCViewController.m
//  Calculator
//
//  Created by Thomas Roughton on 1/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import "CALCViewController.h"
#import "CALCEquationEvaluator.h"
#import "CALCButton.h"
#import "CALCEquationHolder.h"
#import "CALCFunctionSwiperView.h"

#import <QuartzCore/QuartzCore.h>

#define MARGIN 10.0

@interface CALCViewController ()

@property (nonatomic, strong) CALCEquationHolder *equationHolder;
@property (nonatomic, strong) UILabel *displayLabel;
@property (nonatomic, strong) CALCFunctionSwiperView *functionSwiperView;

@end

@implementation CALCViewController

-(void) setupButtons { //Add all the buttons to the screen.
    
    const NSArray *buttonsArray = @[[CALCButton calcButtonWithType:CALCButtonTypeClear],
                                    [CALCButton calcButtonWithType:CALCButtonTypeOpenBracket],
                                    [CALCButton calcButtonWithType:CALCButtonTypeCloseBracket],
                                    [CALCButton operatorButtonWithOperator:[CALCOperator tenToTheOperator]],
                                    [CALCButton functionButtonWithFunctionType:CALCFunctionInverseSign],
                                    [CALCButton operatorButtonWithOperator:[CALCOperator exponentOperator]],
                                    [CALCButton operatorButtonWithOperator:[CALCOperator divisionOperator]],
                                    [CALCButton operatorButtonWithOperator:[CALCOperator multiplicationOperator]],
                                    [CALCButton numericButtonWithDigit:@"7"],
                                    [CALCButton numericButtonWithDigit:@"8"],
                                    [CALCButton numericButtonWithDigit:@"9"],
                                    [CALCButton operatorButtonWithOperator:[CALCOperator subtractionOperator]],
                                    [CALCButton numericButtonWithDigit:@"4"],
                                    [CALCButton numericButtonWithDigit:@"5"],
                                    [CALCButton numericButtonWithDigit:@"6"],
                                    [CALCButton operatorButtonWithOperator:[CALCOperator additionOperator]],
                                    [CALCButton numericButtonWithDigit:@"1"],
                                    [CALCButton numericButtonWithDigit:@"2"],
                                    [CALCButton numericButtonWithDigit:@"3"],
                                    [CALCButton calcButtonWithType:CALCButtonTypeEvaluate],
                                    [CALCButton numericButtonWithDigit:@"0"], 
                                    [CALCButton calcButtonWithType:CALCButtonTypeDecimalPoint],
                                    [CALCButton calcButtonWithType:CALCButtonTypeBackspace]
                                    ]; //An immutable array of all the buttons in the order they'll be laid out.
    CGFloat buttonHeightWMargin = (self.view.bounds.size.height - 2*MARGIN)/8; //The screen has eight sections of equal height.
    CGFloat buttonWidthWMargin = (self.view.bounds.size.width - MARGIN-5.0)/4; 
    CGFloat startingPoint = MARGIN+2*buttonHeightWMargin; //Start after the function switcher
    
    for (int y = 0; y < 6; y++) {
        for (int x = 0; x < 4; x++) {
            if (x+4*y >= buttonsArray.count) break; //We're at the end of the array, so skip ahead.
            
            CALCButton *currentButton = buttonsArray[x+4*y];
            CGFloat height = currentButton.calcButtonType == CALCButtonTypeEvaluate ? 2*buttonHeightWMargin-5.f : buttonHeightWMargin - 5.f ; //Equals button is double height
            
            currentButton.frame = CGRectMake(MARGIN + x*buttonWidthWMargin, startingPoint + y*buttonHeightWMargin, buttonWidthWMargin - 5.f, height);
            
            [currentButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside]; //When there's a TouchUpInside event on the button, call the method buttonPressed on self.
            
            [self.view addSubview:currentButton]; //And add the button to the screen.
        }
    }
    
}

-(void) setupSwiperScrollView { //Add the swiper to the screen.
    
    NSArray *buttons = @[[CALCButton mathsConstantButtonWithConstant:[CALCMathsConstant pi]],
                         [CALCButton mathsConstantButtonWithConstant:[CALCMathsConstant goldenRatio]],
                         [CALCButton mathsConstantButtonWithConstant:[CALCMathsConstant eulersConstant]],
                         [CALCButton functionButtonWithFunctionType:CALCFunctionSine],
                         [CALCButton functionButtonWithFunctionType:CALCFunctionCosine],
                         [CALCButton functionButtonWithFunctionType:CALCFunctionTangent],
                         [CALCButton functionButtonWithFunctionType:CALCFunctionLog]
                         ]; //All the buttons that go in the swiper
    
    for (CALCButton *button in buttons) { //Configure the buttons to go in the swiper 
        [button setInFunctionSwitcher:YES];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    CGFloat buttonHeightWMargin = (self.view.bounds.size.height - 2*MARGIN)/8; 
    CGFloat startingPoint = MARGIN+buttonHeightWMargin;
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(MARGIN, startingPoint + 2.f, self.view.bounds.size.width-MARGIN*2, (self.view.bounds.size.height - MARGIN*2)/8 - 9.f)];
    self.functionSwiperView = [[CALCFunctionSwiperView alloc] initWithFrame:CGRectMake(0, 0, (self.view.bounds.size.width-MARGIN*2)*2, scrollView.bounds.size.height)];
    _functionSwiperView.buttons = buttons;
    scrollView.contentSize = _functionSwiperView.bounds.size;
    
    CALayer *scrollViewLayer = scrollView.layer;
    [scrollViewLayer setMasksToBounds:YES];
    
    [scrollViewLayer setCornerRadius:10.0];
    
    [scrollViewLayer setBorderWidth:2.0];
    [scrollViewLayer setBorderColor:[UIColor lightGrayColor].CGColor];
    
    scrollView.pagingEnabled = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.bounces = NO;
    
    
    [scrollView addSubview:_functionSwiperView];
    [self.view addSubview:scrollView];
    
    
}


- (void)viewDidLoad //Called once the view controller's view has loaded.
{
    [super viewDidLoad]; //It is necessary to call super on any method which you subclass but still want to retain the superclass' functionality.
    
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithHue:0.6 saturation:0.8 brightness:0.6 alpha:1.0]; //Which is a bluey colour.
    
    [self setupSwiperScrollView];
    [self setupButtons];
    
    self.equationHolder = [[CALCEquationHolder alloc] init];
    
    { //Set up the display label. Bracketed off in scope only for ease of reading.
        self.displayLabel = [[UILabel alloc] initWithFrame:CGRectMake(MARGIN, MARGIN, self.view.bounds.size.width-MARGIN*2, (self.view.bounds.size.height - MARGIN*2)/8 - 5.f)];
        _displayLabel.backgroundColor = [UIColor whiteColor];
        _displayLabel.textColor = [UIColor blackColor];
        _displayLabel.font = [UIFont systemFontOfSize:22.f];
        _displayLabel.textAlignment = NSTextAlignmentRight;
        _displayLabel.lineBreakMode = NSLineBreakByTruncatingHead;
    }
    
    [self.view addSubview:_displayLabel];
    
    
}

//Buttons needed: 0,1,2,3,4,5,6,7,8,9,., function chooser/maths constant popup button, +, -, *, /, +/-, E, (, ), backspace, clear, = 

-(void) buttonPressed:(CALCButton*)sender { //Called when any button is pressed, taking the button as a parameter.
    [_equationHolder buttonPressed:sender]; //Pass off the event to the model (in the MVC design pattern)
    self.displayLabel.text = _equationHolder.equationString; //then assign the display's text to the equation string.
}

/*
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 */

@end
