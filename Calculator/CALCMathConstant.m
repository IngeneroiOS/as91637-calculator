//
//  CALCMathConstants.m
//  Calculator
//
//  Created by Thomas Roughton on 3/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import "CALCMathConstant.h"

@implementation CALCMathsConstant

+(CALCMathsConstant*) pi { //Returns a maths constant representing π.
    static CALCMathsConstant *pi = nil;
    static dispatch_once_t onceToken;
dispatch_once(&onceToken, ^{
    pi = [[CALCMathsConstant alloc] init];
    pi.numericValue = [NSDecimalNumber decimalNumberWithMantissa:3141592653589793238ull exponent:-18 isNegative:NO];
    pi.stringValue = @"π";
});
    return pi;
}

+(CALCMathsConstant*) eulersConstant { //Returns a maths constant representing euler's constant.
    static CALCMathsConstant *e = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        e = [[CALCMathsConstant alloc] init];
        e.numericValue = [NSDecimalNumber decimalNumberWithMantissa:2718281828459045235ull exponent:-18 isNegative:NO];
        e.stringValue = @"e";
    });
    return e;
}

+(CALCMathsConstant*) goldenRatio { //Returns a maths constant representing the golden ratio.
    static CALCMathsConstant *goldenRatio = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        goldenRatio = [[CALCMathsConstant alloc] init];
        goldenRatio.numericValue = [NSDecimalNumber decimalNumberWithMantissa:16180339887498948482ull exponent:-19 isNegative:NO];
        goldenRatio.stringValue = @"φ";
    });
    return goldenRatio;
}

@end
