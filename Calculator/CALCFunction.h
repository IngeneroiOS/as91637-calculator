//
//  CALCFunctionWrapper.h
//  Calculator
//
//  Created by Thomas Roughton on 3/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum CALCFunctionType {
    CALCFunctionLog,
    CALCFunctionInverseSign,
    CALCFunctionSine,
    CALCFunctionCosine,
    CALCFunctionTangent
} CALCFunctionType;

@interface CALCFunction : NSObject

+(CALCFunction*) functionWithType:(CALCFunctionType)type equation:(NSArray*)equation;
+(CALCFunction*) logFunctionWithEquation:(NSArray*)equation;
+(CALCFunction*) inverseSignFunctionWithEquation:(NSArray*)equation;
+(CALCFunction*) sineFunctionWithEquation:(NSArray*)equation;
+(CALCFunction*) cosineFunctionWithEquation:(NSArray*)equation;
+(CALCFunction*) tangentFunctionWithEquation:(NSArray*)equation;

-(NSDecimalNumber*) decimalNumberValue;

@property (nonatomic, copy) NSDecimalNumber * (^evaluateFunction)(NSArray* rhs);
@property (nonatomic, strong) NSString *stringValue, *buttonString;
@property (nonatomic, strong) NSMutableArray *subEquation;
@property (nonatomic) BOOL showBrackets;

@end
