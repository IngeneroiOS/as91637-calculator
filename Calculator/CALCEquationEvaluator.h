//
//  CALCEquationEvaluator.h
//  Calculator
//
//  Created by Thomas Roughton on 2/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CALCFunction.h"
#import "CALCOperator+BasicArithmetic.h"
#import "CALCMathConstant.h"

/*  This class holds the app's primary logic. It's designed as a singleton, as it exists for the app's lifecycle.
    It can take an equation array and evaluate it, returning the result as an NSDecimalNumber.
 
    The equation array exists in the following format:
    - The array contents are in the order that the user entered it
    - Numeric values are represented by NSDecimalNumber objects.
    - Operators (e.g. +, -, *, /) are represented by instances of the CALCOperator class.
    - Functions are represented by instances of the CALCFunction class. The arguments that function takes are in the function's subEquation member variable, which is in itself a fully fledged equation.
    - Constants (e.g. π) are represented by instances of the CALCMathConstant class.
    - Brackets are represented by sub-arrays which follow the above format.
 */

@interface CALCEquationEvaluator : NSObject

-(NSDecimalNumber*) evaluateEquation:(NSArray*)equationArray;

+(CALCEquationEvaluator*) sharedEquationEvaluator;

@end
