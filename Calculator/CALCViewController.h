//
//  CALCViewController.h
//  Calculator
//
//  Created by Thomas Roughton on 1/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import <UIKit/UIKit.h>

/* This is the view controller for the main view. It doesn't need to expose any methods or instance variables; hence the empty interface */

@interface CALCViewController : UIViewController

@end
