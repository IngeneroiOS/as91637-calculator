//
//  CALCOperator.h
//  Calculator
//
//  Created by Thomas Roughton on 2/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum CALCOperatorPrecedence : NSUInteger {
    CALCOperatorPrecedenceEquality = 1,
    CALCOperatorPrecedenceAdditionSubtraction = 2,
    CALCOperatorPrecedenceMultiplicationDivision,
    CALCOperatorPrecedenceExponents
} CALCOperatorPrecedence;

@interface CALCOperator : NSObject

+(CALCOperator*) equalityOperator;
+(CALCOperator*) exponentOperator;
+(CALCOperator*) tenToTheOperator;

-(NSDecimalNumber*)evaluateLeftHandSide:(NSDecimalNumber*)lhs andRightHandSide:(NSDecimalNumber*)rhs;

@property (nonatomic, strong) NSString *stringValue; //If this operator needs to be expressed in a string
@property (nonatomic) CALCOperatorPrecedence precedence; //Used to determine order of operations. A higher precedence means it should be calculated first. This code follows the BEDMAS order of operations, with equality (if implemented) being assigned a precedence of 1, addition and subtraction a precedence of 2, multiplication and division 3, exponents (if implemented) 4, and parentheses (if implemented) 5
@property (nonatomic, copy) NSDecimalNumber * (^evaluateExpression)(NSDecimalNumber* lhs, NSDecimalNumber* rhs);

@end
