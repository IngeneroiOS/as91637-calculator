//
//  CALCButton.m
//  Calculator
//
//  Created by Thomas Roughton on 4/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//
// Adapted from a tutorial by Ray Wenderlich, as found on http://www.raywenderlich.com/2134/core-graphics-101-glossy-buttons
//

#import "DrawingFunctions.h"

CGRect rectFor1PxStroke(CGRect rect) { //Returns a rect with a one pixel margin
    return CGRectMake(rect.origin.x + 0.5, rect.origin.y + 0.5, rect.size.width - 1, rect.size.height - 1);
}

void drawLinearGradient(CGContextRef context, CGRect rect, UIColor* startColour, UIColor*  endColour) { //Draws a linear gradient in the specified rectangle on the context with the startColour and endColour
    CGColorSpaceRef colourSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colours = @[(id)[startColour CGColor], (id)[endColour CGColor]];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colourSpace, (__bridge CFArrayRef)(colours), locations);
    CGColorSpaceRelease(colourSpace);
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextSaveGState(context);
    CGContextAddRect(context, rect);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    CGGradientRelease(gradient);
}

void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef colour) { //Draws a 1px line of colour colour from startPoint to endPoint on the context.
    
    CGContextSaveGState(context);
    CGContextSetLineCap(context, kCGLineCapSquare);
    CGContextSetStrokeColorWithColor(context, colour);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, startPoint.x + 0.5, startPoint.y + 0.5);
    CGContextAddLineToPoint(context, endPoint.x + 0.5, endPoint.y + 0.5);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);        
    
}

void drawGlossAndGradient(CGContextRef context, CGRect rect, UIColor* startColour, UIColor* endColour) { //Draws a gloss effect over a gradient
    
    drawLinearGradient(context, rect, startColour, endColour);
    
    UIColor* glossColor1 = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.35];
    UIColor* glossColor2 = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.1];
    
    CGRect topHalf = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height/2);
    
    drawLinearGradient(context, topHalf, glossColor1, glossColor2);
    
}

CGMutablePathRef createRoundedRectForRect(CGRect rect, CGFloat radius) { //Returns the path of a rect with rounded corners of radius radius.
    
    CGMutablePathRef newPath = CGPathCreateMutable();
    CGPathMoveToPoint(newPath, NULL, CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPathAddArcToPoint(newPath, NULL, CGRectGetMaxX(rect), CGRectGetMinY(rect), CGRectGetMaxX(rect), CGRectGetMaxY(rect), radius);
    CGPathAddArcToPoint(newPath, NULL, CGRectGetMaxX(rect), CGRectGetMaxY(rect), CGRectGetMinX(rect), CGRectGetMaxY(rect), radius);
    CGPathAddArcToPoint(newPath, NULL, CGRectGetMinX(rect), CGRectGetMaxY(rect), CGRectGetMinX(rect), CGRectGetMinY(rect), radius);
    CGPathAddArcToPoint(newPath, NULL, CGRectGetMinX(rect), CGRectGetMinY(rect), CGRectGetMaxX(rect), CGRectGetMinY(rect), radius);
    CGPathCloseSubpath(newPath);
    return newPath;  
}