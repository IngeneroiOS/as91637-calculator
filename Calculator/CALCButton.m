//
//  CALCButton.m
//  Calculator
//
//  Created by Thomas Roughton on 4/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//
// Drawing code adapted from a tutorial by Ray Wenderlich, as found on http://www.raywenderlich.com/2134/core-graphics-101-glossy-buttons
//

#import "CALCButton.h"
#import "CALCOperator.h"
#import "CALCMathConstant.h"
#import "DrawingFunctions.h"

@interface CALCButton ()

@property (nonatomic) CGFloat hue, saturation, brightness;
@end

@implementation CALCButton

#pragma mark Convenience Methods for button creation.

+(CALCButton*) numericButtonWithDigit:(NSString*)digit { //Returns a button with the string presented on it.
    CALCButton *button = [CALCButton calcButtonWithType:CALCButtonTypeNumeric];
    [button setTitle:digit forState:UIControlStateNormal];
    button.tag = [digit intValue];
    return button;
}

+(CALCButton*) operatorButtonWithOperator:(CALCOperator*)operatorObj { //Returns a button for a associated operator
    CALCButton *button = [CALCButton calcButtonWithType:CALCButtonTypeOperator];
    button.associatedObject = operatorObj;
    [button setTitle:operatorObj.stringValue forState:UIControlStateNormal];
    return button;
}

+(CALCButton*) mathsConstantButtonWithConstant:(CALCMathsConstant *)constantObj { //Returns a button for a maths constant
    CALCButton *button = [CALCButton calcButtonWithType:CALCButtonTypeMathsConstant];
    button.associatedObject = constantObj;
    [button setTitle:constantObj.stringValue forState:UIControlStateNormal];
    return button;
}

+(CALCButton*) functionButtonWithFunctionType:(CALCFunctionType)functionType { //Returns a button for a function.
    CALCButton *button = [CALCButton calcButtonWithType:CALCButtonTypeFunction];
    button.functionType = functionType;
    [button setTitle:[CALCFunction functionWithType:functionType equation:nil].buttonString forState:UIControlStateNormal];
    return button;
}

+(CALCButton*) calcButtonWithType:(CALCButtonType)buttonType { //Returns a button of the specified type. The default convenience method for CALCButton.
    static NSString *decimalPointString = nil;
    
    if (!decimalPointString) {
        NSString *tempString = [NSString localizedStringWithFormat:@"%.2f", 1.1f];
        decimalPointString = [NSString stringWithFormat:@"%c", [tempString characterAtIndex:1]]; //Slightly hacky but foolproof way of getting the correct symbol for the decimal point in the current locale.
    }
    
    const NSArray *buttonStrings = @[@"C", @"(", @")", @"⌫", @"Op.", @"Num.", @"f(x)", @"=", decimalPointString, @"Const."]; //This needs to match enum CalcButtonType for the correct symbols to be displayed. It has to be in method scope to work.
    
    CALCButton *button = [[CALCButton alloc] init];
    button.calcButtonType = buttonType;
    button.saturation = 0.f;
    button.brightness = 0.9f;
    [button setTitle:buttonStrings[buttonType] forState:UIControlStateNormal];
   // button.backgroundColor = [UIColor whiteColor];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [button.titleLabel addObserver:button forKeyPath:@"text" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil]; //This gets called whenever the button's state changes. It's an ugly hack, but an unavoidable one. (The buttons stay highlighted if this isn't implemented. Solution found here: http://stackoverflow.com/questions/2503721/how-listen-for-uibutton-state-change
    
    return button;
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"text"]) { //The second half of the earlier ugly hack.
        [self setNeedsDisplay]; //Update the background colour.
    }
}

#pragma mark - Custom Drawing Code

-(void) drawRectFunctionSwitcher:(CGRect)rect { //Drawing code for buttons within the function switcher.
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (self.highlighted) {
        CGContextSetRGBFillColor(context, .8f, .8f, .8f, 1.f);
    } else {
        CGContextSetRGBFillColor(context, 1.f, 1.f, 1.f, 0.f);
    }
    
    CGContextFillRect(context, rect);
}

//Henceforth this is adapted code, so I don't claim it as entirely my own.

-(void) drawRectMainScreenButton:(CGRect)rect { //Drawing code for buttons on the main screen
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (_hue > 1.f || _hue < 0.f || isnan(_hue)) {
        self.hue = 0.f;
    }
    
    CGFloat actualBrightness = _brightness;
    if (self.state == UIControlStateHighlighted) {
        actualBrightness -= 0.10;
    }
    
    UIColor* highlightStart = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4];
    UIColor* highlightStop = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.1];
    
    UIColor* outerTop = [UIColor colorWithHue:_hue saturation:_saturation brightness:actualBrightness alpha:1.0];
    UIColor* outerBottom = [UIColor colorWithHue:_hue saturation:_saturation brightness:0.60*actualBrightness alpha:1.0];
    
    CGFloat outerMargin = 5.0f;
    CGRect outerRect = CGRectInset(self.bounds, outerMargin, outerMargin);
    CGMutablePathRef outerPath = createRoundedRectForRect(outerRect, 2.0); //Get the path for the outer border
    
    CGFloat innerMargin = 3.0f;
    CGRect innerRect = CGRectInset(outerRect, innerMargin, innerMargin);
    CGMutablePathRef innerPath = createRoundedRectForRect(innerRect, 2.0); //Get the path for the inner border
    
    CGFloat highlightMargin = 2.0f;
    CGRect highlightRect = CGRectInset(outerRect, highlightMargin, highlightMargin);
    CGMutablePathRef highlightPath = createRoundedRectForRect(highlightRect, 2.0);
    
    // Draw gradient for outer path
    CGContextSaveGState(context);
    CGContextAddPath(context, outerPath);
    CGContextClip(context);
    drawLinearGradient(context, outerRect, outerTop, outerBottom);
    CGContextRestoreGState(context);
    
    
    // Draw highlight (if not selected)
    if (self.state != UIControlStateHighlighted) {
        CGContextSaveGState(context);
        CGContextSetLineWidth(context, 4.0);
        CGContextAddPath(context, outerPath);
        CGContextAddPath(context, highlightPath);
        CGContextEOClip(context);
        drawLinearGradient(context, outerRect, highlightStart, highlightStop);
        CGContextRestoreGState(context);
    }
    
    CGPathRelease(outerPath);
    CGPathRelease(innerPath);
    CGPathRelease(highlightPath);
}

- (void)drawRect:(CGRect)rect { //Calls the relevant drawing method, depending on whether the button is in the function switcher or not.
    (self.isInFunctionSwitcher) ? [self drawRectFunctionSwitcher:rect] : [self drawRectMainScreenButton:rect];
    
}


@end
