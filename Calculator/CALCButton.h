//
//  CALCButton.h
//  Calculator
//
//  Created by Thomas Roughton on 4/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CALCFunction.h"

@class CALCOperator, CALCMathsConstant;

typedef enum CALCButtonType : NSUInteger {
    CALCButtonTypeClear = 0,
    CALCButtonTypeOpenBracket,
    CALCButtonTypeCloseBracket,
    CALCButtonTypeBackspace,
    CALCButtonTypeOperator,
    CALCButtonTypeNumeric,
    CALCButtonTypeFunction,
    CALCButtonTypeEvaluate,
    CALCButtonTypeDecimalPoint,
    CALCButtonTypeMathsConstant
} CALCButtonType;

@interface CALCButton : UIButton

+(CALCButton*) numericButtonWithDigit:(NSString*)digit;
+(CALCButton*) operatorButtonWithOperator:(CALCOperator*)operatorObj;
+(CALCButton*) functionButtonWithFunctionType:(CALCFunctionType)functionType;
+(CALCButton*) calcButtonWithType:(CALCButtonType)buttonType;
+(CALCButton*) mathsConstantButtonWithConstant:(CALCMathsConstant*)constantObj;

@property (nonatomic, readwrite) CALCButtonType calcButtonType;
@property (nonatomic, readwrite) CALCFunctionType functionType; //If applicable
@property (nonatomic, strong) id associatedObject; //If applicable
@property (nonatomic, getter = isInFunctionSwitcher) BOOL inFunctionSwitcher;

@end
