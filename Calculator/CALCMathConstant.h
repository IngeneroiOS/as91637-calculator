//
//  CALCMathConstants.h
//  Calculator
//
//  Created by Thomas Roughton on 3/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CALCMathsConstant : NSObject

+(CALCMathsConstant*) pi;
+(CALCMathsConstant*) eulersConstant;
+(CALCMathsConstant*) goldenRatio;

@property (nonatomic, strong) NSString *stringValue;
@property (nonatomic, strong) NSDecimalNumber *numericValue;

@end
