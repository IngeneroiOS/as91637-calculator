//
//  CALCFunctionSwiperView.m
//  Calculator
//
//  Created by Thomas Roughton on 15/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import "CALCFunctionSwiperView.h"
#import "DrawingFunctions.h"
#define BUTTONWIDTH 60.0

@implementation CALCFunctionSwiperView

- (id)initWithFrame:(CGRect)frame //Designated initialiser, returning self.
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [UIColor lightGrayColor].CGColor);
    // Draw them with a 2.0 stroke width so they are a bit more visible.
    CGContextSetLineWidth(context, 2.0);
    
    for (CGFloat x = BUTTONWIDTH; x < self.buttons.count*BUTTONWIDTH; x+=BUTTONWIDTH) {
        
        CGContextMoveToPoint(context, x, 2.f); //Start the line at this point,
        
        CGContextAddLineToPoint(context, x, rect.size.height - 2.f); //draw to this point
        
        // and now draw the path
        CGContextStrokePath(context);
    }
    
}

-(void) setButtons:(NSArray *)buttons { //Setup the buttons in the swiper from the array passed in.
    for (UIButton *button in _buttons) {
        [button removeFromSuperview];
    }
    _buttons = buttons;
    CGFloat height = self.bounds.size.height;
    CGFloat workingX = 0;
    
    for (UIButton *button in _buttons) {
        
        button.frame = CGRectMake(workingX, 0, BUTTONWIDTH, height);
        [self addSubview:button];
        workingX += BUTTONWIDTH;
    }
}


@end
