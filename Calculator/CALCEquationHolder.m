//
//  CALCEquationHolder.m
//  Calculator
//
//  Created by Thomas Roughton on 5/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import "CALCEquationHolder.h"
#import "CALCButton.h"
#import "CALCOperator.h"
#import "CALCEquationEvaluator.h"

@interface CALCEquationHolder () {
    NSUInteger _currentDepth;
    BOOL _displayingResult;
    BOOL _inSingleArgFunction;
    BOOL _hasEnteredDPInNumber;
}

@property (nonatomic, strong) NSMutableArray *currentEquation; //Equation that we're currently working on. The first element is a reference to its parent object.
@property (nonatomic, strong) NSMutableArray *enteredStrings; //All the strings that have been entered, for backspace functionality.

@end

@implementation CALCEquationHolder

- (id)init //Initialises the object and returns itself.
{
    self = [super init];
    if (self) {
        [self resetParameters];
        _displayingResult = NO;
    }
    return self;
}

-(void) resetParameters { //Resets most of the instance variables to their initial values.
    self.equation = [NSMutableArray array]; 
    self.currentEquation = _equation;
    self.enteredStrings = [NSMutableArray array];
    self.equationString = [NSMutableString string];
    _currentEquation[0] = [NSNull null];
    _currentDepth = 0;
    _inSingleArgFunction = NO;
}

-(void) clear { //Perform the clear operation (e.g. when clear button is pressed)
    [self resetParameters];
    _hasEnteredDPInNumber = NO;
    _displayingResult = NO;
    
}

-(BOOL) isNumeric:(id)obj { //Numeric has a very loose meaning here. Because the strings aren't converted into numbers until the equals button is pressed, what we're really checking for is whether the last element in the current equation is a string, although math constants are also allowed, along with NSDecimalNumbers and completed functions.

    return ([obj isKindOfClass:[NSString class]] && ![obj isEqualToString:@"Error"])
    ||
    [obj isKindOfClass:[NSDecimalNumber class]]
    ||
    [obj isKindOfClass:[CALCMathsConstant class]]
    ||
    ([obj isKindOfClass:[CALCFunction class]] && [self isNumeric:[[obj subEquation] lastObject]]);
}


-(BOOL) lastItemIsNumeric { //Get the last non-array object, and check whether it's numeric.
    id obj = [_currentEquation lastObject];
    id initialObj = obj;
    while ([obj isKindOfClass:[NSArray class]]) { //Set obj to the last object that isn't an NSArray, because brackets aren't what we're checking for.
        
        obj = [obj lastObject];
        if (obj == initialObj) { //In case we get circular referencing (e.g. two brackets being the only thing entered).
            break;
        }
    }
    return [self isNumeric:obj];
}

-(void) buttonPressed:(CALCButton*)sender { //Called when a button is pressed.
    
    switch (sender.calcButtonType) { //One case for each button type is necessarily long, but I've tried to keep the code within them short.
            
        case CALCButtonTypeDecimalPoint: {
            if (_hasEnteredDPInNumber && [self lastItemIsNumeric]) {  //If there's already a decimal point in this number
                break; 
            }
            _hasEnteredDPInNumber = YES; //Continue onto the next case; the break statement has been purposely ommited.
        }
        case CALCButtonTypeNumeric: {
            if (_displayingResult) { //If we're still displaying the result from the last equation, clear it.
                [self clear];
            }
            id obj = [_currentEquation lastObject];
            if ([obj isKindOfClass:[NSString class]]) { //If the last object was a numeric string which we can simply append onto. N.B. we can't use the lastItemIsNumeric method because that also checks whether the last object is an NSDecimalNumber, which we can't use here.
                
                obj = [obj stringByAppendingString:sender.titleLabel.text];
                [_currentEquation replaceObjectAtIndex:_currentEquation.count - 1 withObject:obj];
                
            } else {
                
                [_currentEquation addObject:sender.titleLabel.text];
                
            }
            [self enterString:sender.titleLabel.text];
        }
            break;
        case CALCButtonTypeOpenBracket: {
            
            if (![self lastItemIsNumeric]) { //If either an operator was entered last, an open bracket was entered last or we're at the start of the array
                NSMutableArray *newSubEquation = [NSMutableArray array];
                [self enterEquation:newSubEquation];
                [self enterString:sender.titleLabel.text];
            }
        }
            break;
        case CALCButtonTypeCloseBracket: {
            
            if (_currentDepth > 0 && [self lastItemIsNumeric]) { //If a number was entered last and we're not already at the top depth.
                [self exitEquation];
                [self enterString:sender.titleLabel.text];
            }
        }
            break;
        case CALCButtonTypeBackspace: { 

            if (_currentEquation.count > 1) {
                
                [self deleteLastCharacter];
                
            } else if (_currentDepth > 0) {
                
                self.currentEquation = _currentEquation[0];
                _currentDepth--;
                
                [self deleteLastCharacter];
            }
            
        }
            break;
        case CALCButtonTypeClear: {
            [self clear];
            
        }
            break;
        case CALCButtonTypeEvaluate: {
            _inSingleArgFunction = NO;
            [self evaluateEquation];
        }
            break;
        case CALCButtonTypeFunction: {
            if (![self lastItemIsNumeric]) {
                CALCFunction *function = [CALCFunction functionWithType:sender.functionType equation:[NSMutableArray array]];
                function.subEquation[0] = _currentEquation;
                [_currentEquation addObject:function];
                self.currentEquation = function.subEquation;
                if(function.showBrackets) _currentDepth++;
                else _inSingleArgFunction = YES;
                
                [self enterString:function.stringValue];
            }
        }
            break;
        case CALCButtonTypeOperator: {
            if ([self lastItemIsNumeric]) {
                _displayingResult = NO;
                if (_inSingleArgFunction) {
                    [self exitEquationDecrementingDepth:NO];
                    _inSingleArgFunction = NO;
                }
                CALCOperator *operator = sender.associatedObject;
                [_currentEquation addObject:operator];
                [self enterString:operator.stringValue];
            }
        }
            break;
        case CALCButtonTypeMathsConstant: {
            if (_displayingResult) { //If we're still displaying the result from the last equation, clear it.
                [self clear];
            }
            if (![self lastItemIsNumeric]) {
                [_currentEquation addObject:sender.associatedObject];
                [self enterString:[sender.associatedObject stringValue]];
            }
            
        }
            break;
            
        default:
            break;
    }
}

-(void) deleteLastCharacter { //Delete the last character in the last string entered.
    
    id obj = [_currentEquation lastObject];
    if ([obj isKindOfClass:[NSString class]] && [obj length] > 0) {
        
        obj = [obj substringToIndex:[obj length] - [[_enteredStrings lastObject] length]];
        [_currentEquation replaceObjectAtIndex:_currentEquation.count - 1 withObject:obj];
        
        if ([obj rangeOfString:@"."].location == NSNotFound && [obj rangeOfString:@","].location == NSNotFound) { //If there's now no decimal point
            _hasEnteredDPInNumber = NO;
        }
    } else [_currentEquation removeLastObject];
    
    if (_equationString.length >= [_enteredStrings.lastObject length]) {
        [_equationString deleteCharactersInRange:NSMakeRange(_equationString.length - [[_enteredStrings lastObject] length], [[_enteredStrings lastObject] length])];
    }
    [_enteredStrings removeLastObject];
}

-(void) enterString:(NSString*) string { //Add a string to the equation
    _enteredStrings[_enteredStrings.count] = string;
    [_equationString appendString:string];
}

-(void) enterEquation:(NSMutableArray*)equation {
    
    /*  An equation is an array, and brackets each contain their own equation (i.e. a ( signifies the start of an array).
        As NSArray's are inherently one-dimensional, I keep track of the current depth in a variable, and keep a reference to the parent array as the first element of the current equation. In this method, we need to enter a new equation, setting up the reference to the parent equation. */
    
    _currentEquation[_currentEquation.count] = equation;
    equation[0] = _currentEquation;
    self.currentEquation = equation;
    _currentDepth++;
}

-(BOOL) exitEquationDecrementingDepth:(BOOL) decrement { //Does the opposite of enterEquation
    if (_currentDepth > 0 || !decrement) {
        _currentEquation = _currentEquation[0];
        if (decrement) _currentDepth--;
        return YES;
    } else return NO;
}

-(BOOL) exitEquation { //Convenience wrapper for exitEquationDecrementingDepth.
    return [self exitEquationDecrementingDepth:YES];
}

-(void) evaluateEquation { //When the equals button is pressed, this method assigns the result of the equation to the equationString ivar.
    @try {
        [self formatEquation:self.equation];
        CALCEquationEvaluator *evaluator = [CALCEquationEvaluator sharedEquationEvaluator];
        NSDecimalNumber *result = [evaluator evaluateEquation:self.equation];
        NSString *resultString = [result stringValue];
        _displayingResult = YES;
        [self resetParameters];
        if (resultString) [_equation addObject:resultString];
        self.equationString = [resultString mutableCopy];
    }
    @catch (NSException *exception) {
        //Tell the user what the exception was.
        NSMutableString *exceptionString = [exception.reason mutableCopy];
        
        [exceptionString replaceOccurrencesOfString:@"NSDecimalNumber " withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, @"NSDecimalNumber ".length)];
        NSString *firstCharacter = [exceptionString substringToIndex:1];
        [exceptionString replaceCharactersInRange:NSMakeRange(0, 1) withString:[firstCharacter uppercaseString]];
        
        [self resetParameters];
        self.equationString = exceptionString;
        [self.enteredStrings addObject:_equationString];
        _displayingResult = YES;
    }
    
}

-(void) formatEquation:(NSMutableArray*)equation { //Remove the first object and replace strings with NSDecimalNumbers
    
   [equation removeObjectAtIndex:0]; //Get rid of the reference to its parent array. Not strictly necessary, but a good idea in case later code introduces compatibility issues.
    
    for (int index = 0; index < equation.count; index++) { //Use a simple for loop, as Obj-C fast enumeration won't let you mutate an array while enumerating it.
        id obj = [equation objectAtIndex:index];
        if ([obj isKindOfClass:[NSString class]]) {
            equation[index] = [NSDecimalNumber decimalNumberWithString:obj];
        } else if ([obj isKindOfClass:[NSArray class]]) {
            [self formatEquation:obj];
        } else if ([obj isKindOfClass:[CALCFunction class]]) {
            [self formatEquation:[obj subEquation]];
        }
    }
}

@end
