//
//  CALCOperator+BasicArithmetic.m
//  Calculator
//
//  Created by Thomas Roughton on 2/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import "CALCOperator+BasicArithmetic.h"

@implementation CALCOperator (BasicArithmetic)

+(CALCOperator*) additionOperator { //Returns the singleton instance of the addition operator.
    static CALCOperator *additionOperator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        additionOperator = [[CALCOperator alloc] init];
        additionOperator.precedence = CALCOperatorPrecedenceAdditionSubtraction;
        additionOperator.stringValue = @"+";
        additionOperator.evaluateExpression = ^ NSDecimalNumber* (NSDecimalNumber* lhs, NSDecimalNumber* rhs){
            return [lhs decimalNumberByAdding:rhs];
        };
    });
    return additionOperator;
}

+(CALCOperator*) subtractionOperator { //Returns the singleton instance of the subtraction operator.
    static CALCOperator *subtractionOperator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        subtractionOperator = [[CALCOperator alloc] init];
        subtractionOperator.precedence = CALCOperatorPrecedenceAdditionSubtraction;
        subtractionOperator.stringValue = @"−";
        subtractionOperator.evaluateExpression = ^ NSDecimalNumber* (NSDecimalNumber* lhs, NSDecimalNumber* rhs){
            return [lhs decimalNumberBySubtracting:rhs];
        };
    });
    return subtractionOperator;
}

+(CALCOperator*) multiplicationOperator { //Returns the singleton instance of the multiplication operator.
    static CALCOperator *multiplicationOperator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        multiplicationOperator = [[CALCOperator alloc] init];
        multiplicationOperator.precedence = CALCOperatorPrecedenceMultiplicationDivision;
        multiplicationOperator.stringValue = @"×";
        multiplicationOperator.evaluateExpression = ^ NSDecimalNumber* (NSDecimalNumber* lhs, NSDecimalNumber* rhs){
            return [lhs decimalNumberByMultiplyingBy:rhs];
        };
    });
    return multiplicationOperator;
}

+(CALCOperator*) divisionOperator { //Returns the singleton instance of the division operator.
    static CALCOperator *divisionOperator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        divisionOperator = [[CALCOperator alloc] init];
        divisionOperator.precedence = CALCOperatorPrecedenceMultiplicationDivision;
        divisionOperator.stringValue = @"÷";
        divisionOperator.evaluateExpression = ^ NSDecimalNumber* (NSDecimalNumber* lhs, NSDecimalNumber* rhs){
            return [lhs decimalNumberByDividingBy:rhs];
        };
    });
    return divisionOperator;
}


@end
