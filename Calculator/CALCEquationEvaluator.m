
//
//  CALCEquationEvaluator.m
//  Calculator
//
//  Created by Thomas Roughton on 2/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//
//  Used as reference: http://en.wikipedia.org/wiki/Operator-precedence_parser
//


#import "CALCEquationEvaluator.h"
#import "CALCOperator.h"
#import "CALCOperator+BasicArithmetic.h"
#import "CALCFunction.h"
#import "CALCMathConstant.h"

@interface CALCEquationEvaluator ()

@end

@implementation CALCEquationEvaluator

static inline NSDecimalNumber* wrapValue(int value) {
    int positiveValue = abs(value);
    BOOL isNegative = (positiveValue == value);
    
    return [NSDecimalNumber decimalNumberWithMantissa:positiveValue exponent:0 isNegative:isNegative];
}

#pragma mark - Singleton initialisation and sample equation

+(CALCEquationEvaluator*) sharedEquationEvaluator { //Returns the singleton instance of CALCEquationEvaluator.
    static CALCEquationEvaluator *sharedEquationEvaluator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedEquationEvaluator = [[CALCEquationEvaluator alloc] init];
       //[sharedEquationEvaluator performSelector:@selector(evaluateSampleEquation) withObject:nil afterDelay:1.0];
    });
     
    return sharedEquationEvaluator;
    
}

-(void) evaluateSampleEquation {
    // The sample equation is: (log(4^2)/log(2) + 3 ) / 7 - 5*(1/2) = -1.5
    // This equation can be used to test the functionality of the algorithm.
    
    CALCFunction *logWrapperOne = [CALCFunction logFunctionWithEquation:@[wrapValue(4), [CALCOperator exponentOperator], [NSDecimalNumber decimalNumberWithString:@"2"]]];
    CALCFunction *logWrapperTwo = [CALCFunction logFunctionWithEquation:@[wrapValue(2)]];
    
    NSArray *sampleEquation = @[@[logWrapperOne, [CALCOperator divisionOperator], logWrapperTwo, [CALCOperator additionOperator], wrapValue(3)],
                                [CALCOperator divisionOperator], wrapValue(7),
                                [CALCOperator subtractionOperator],
                                wrapValue(5), [CALCOperator multiplicationOperator], @[[NSDecimalNumber one], [CALCOperator divisionOperator], wrapValue(2)]];
    
    NSLog(@"%@ = %@", [self equationToString:sampleEquation], [self evaluateEquation:sampleEquation].stringValue);
    
}

#pragma mark - Array Iterators

-(CALCOperator*) nextOperatorInArray:(NSArray*)array fromIndex:(NSUInteger)index indexFound:(NSUInteger*)indexFound { //Returns the first operator found in the array, starting from the index passed in. Also returns the index the operator was found at.
    
    if (index >= array.count) { //If we're looking beyond the array bounds.
        return nil;
    }
    

    for (; index < array.count; index++) { //Iterate through each object in the array. We're using a C for-loop rather than Objective-C fast enumeration as we want to retain the index, and we may start at a higher index than zero.
        id obj = [array objectAtIndex:index];
        if ([obj isKindOfClass:[CALCOperator class]]) {
            if (indexFound != nil) *indexFound = index;
            return obj;
        }
    }
    return nil; //No more operators
}

-(NSDecimalNumber*) nextNumberInArray:(NSArray*)array fromIndex:(NSUInteger)index indexFound:(NSUInteger*)indexFound { //Returns the first number found in the array, starting from the index passed in. Also returns the index the operator was found at.
    if (index >= array.count) { //If we're looking beyond the array bounds.
        return nil;
    }
    
    for (; index < array.count; index++) {
        id obj = [array objectAtIndex:index];
        if ([obj isKindOfClass:[NSDecimalNumber class]]) {
            if (indexFound != nil) *indexFound = index;
            return obj;
        }
    }
    return nil;
}

#pragma mark - Algorithms for evaluation.

//Brackets are always evaluated first, then functions, which call this method recursively. The algorithm is based on Wikipedia's pseudo-code for an Operator Precedence Parser.

-(NSDecimalNumber*)evaluateEquation:(NSArray *)equationArray startingIndex:(const NSUInteger)startingIndex minimumPrecedence:(const CALCOperatorPrecedence)minPrecedence { //Evaluate the equation passed in, starting from startingIndex and looking for operators of higher precedence than minPrecedence. Returns a decimal number.
    
    NSDecimalNumber *leftHandSide = [self nextNumberInArray:equationArray fromIndex:startingIndex indexFound:nil]; //Get the first number
    NSUInteger workingIndex = startingIndex - 1; //Start looking one back, as it's incremented each time.
    while ([self nextOperatorInArray:equationArray fromIndex:workingIndex + 1 indexFound:nil].precedence >= minPrecedence) {
        
        CALCOperator *currentOperator = [self nextOperatorInArray:equationArray fromIndex:workingIndex + 1 indexFound:&workingIndex];
        if (!currentOperator) break; //We've reached the end of the array
        
        NSUInteger indexOfRHS = workingIndex;
        NSDecimalNumber *rightHandSide = [self nextNumberInArray:equationArray fromIndex:indexOfRHS indexFound:&indexOfRHS];
    
        
        while ([self nextOperatorInArray:equationArray fromIndex:workingIndex + 1 indexFound:nil].precedence > currentOperator.precedence) {
            
            CALCOperator *nextOperator = [self nextOperatorInArray:equationArray fromIndex:workingIndex + 1 indexFound:&workingIndex];
            
            if (!nextOperator) break; 
            
            rightHandSide = [self evaluateEquation:equationArray startingIndex:indexOfRHS minimumPrecedence:nextOperator.precedence];
        }
        
        leftHandSide = [currentOperator evaluateLeftHandSide:leftHandSide andRightHandSide:rightHandSide];
        
    }
    
    
    return leftHandSide;
}

-(NSArray*) flattenEquation:(NSArray*) equationArray { //Make a copy of the array passed in, converting any bracketed sub-equations, constants or functions to NSDecimalNumbers. The final array returned contains only CALCOperators and NSDecimalNumbers.
    NSMutableArray *workingEquation = [NSMutableArray arrayWithCapacity:equationArray.count];
    for (id equationComponent in equationArray) {
        if ([equationComponent isKindOfClass:[NSDecimalNumber class]] || [equationComponent isKindOfClass:[CALCOperator class]]) {
            [workingEquation addObject:equationComponent];
        } else if ([equationComponent isKindOfClass:[NSArray class]]) {
            [workingEquation addObject:[self evaluateEquation:equationComponent]];
        } else if ([equationComponent isKindOfClass:[CALCFunction class]]) {
            [workingEquation addObject:[(CALCFunction*)equationComponent decimalNumberValue]];
        } else if ([equationComponent isKindOfClass:[CALCMathsConstant class]]) {
            [workingEquation addObject:[(CALCMathsConstant*)equationComponent numericValue]];
        }
    }
    return workingEquation;
}

-(NSDecimalNumber*) evaluateEquation:(NSArray *)equationArray { //Evaluates an equation, returning a decimal number.
    equationArray = [self flattenEquation:equationArray];
    NSDecimalNumber *result = [self evaluateEquation:(NSArray*)equationArray startingIndex:0 minimumPrecedence:0];
    
   //Optional: Round the decimal to fit the display and remove any inaccuracy added by floating point values (e.g. pow, log).
    static NSDecimalNumberHandler *handler = nil;
    if (!handler) {
        handler = [[NSDecimalNumberHandler alloc] initWithRoundingMode:NSRoundPlain scale:22 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO]; //We don't want to raise any exceptions, but rather continue after an error. Any errors in the maths will have already been caught.
    }
    result = [result decimalNumberByRoundingAccordingToBehavior:handler];
    
    return result;
}

-(NSString*) equationToString:(NSArray*) equation { //Returns a string representation of the equation array passed in.
    NSMutableString *string = [NSMutableString string];
    for (id obj in equation) {
        if ([obj isKindOfClass:[NSDecimalNumber class]]) {
            [string appendString:[(NSDecimalNumber*)obj stringValue]];
        } else if ([obj isKindOfClass:[CALCOperator class]]) {
            [string appendString:[(CALCOperator*)obj stringValue]];
        } else if ([obj isKindOfClass:[NSArray class]]) {
            [string appendFormat:@"(%@)", [self equationToString:obj]];
        } else if ([obj isKindOfClass:[CALCFunction class]]) {
            CALCFunction *functionWrapper = obj;
            [string appendFormat:@"%@(%@)", functionWrapper.stringValue, [self equationToString:functionWrapper.subEquation]];
        } else if ([obj isKindOfClass:[CALCMathsConstant class]]) {
            [string appendString:[obj stringValue]];
        }
    }
    return string;
}


@end
