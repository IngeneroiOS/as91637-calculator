//
//  CALCOperator.m
//  Calculator
//
//  Created by Thomas Roughton on 2/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import "CALCOperator.h"

@interface CALCOperator ()

@end

@implementation CALCOperator

/* An example of an extension that could be added */

+(CALCOperator*) equalityOperator { //Returns the singleton instance of the equality operator.
    static CALCOperator *equalityOperator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        equalityOperator = [[CALCOperator alloc] init];
        equalityOperator.precedence = CALCOperatorPrecedenceEquality;
        equalityOperator.stringValue = @"⩵";
        equalityOperator.evaluateExpression = ^ NSDecimalNumber* (NSDecimalNumber* lhs, NSDecimalNumber* rhs){
            return [NSDecimalNumber decimalNumberWithDecimal:@([lhs isEqualToNumber:rhs]).decimalValue];
        };
    });
    return equalityOperator;
}

+(CALCOperator*) exponentOperator { //Returns the singleton instance of the exponent operator.
    static CALCOperator *exponentOperator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        exponentOperator = [[CALCOperator alloc] init];
        exponentOperator.precedence = CALCOperatorPrecedenceExponents;
        exponentOperator.stringValue = @"^";
        exponentOperator.evaluateExpression = ^ NSDecimalNumber* (NSDecimalNumber* lhs, NSDecimalNumber* rhs){
            if ([rhs isEqualToNumber:[NSDecimalNumber decimalNumberWithMantissa:5 exponent:1 isNegative:NO]]) {
                return [NSDecimalNumber decimalNumberWithDecimal:@(sqrt(lhs.doubleValue)).decimalValue];
            }
            
            if (rhs.integerValue > 0 && rhs.integerValue == rhs.doubleValue) {
                return [lhs decimalNumberByRaisingToPower:rhs.unsignedIntegerValue];
            }
            return [NSDecimalNumber decimalNumberWithDecimal:@(pow(lhs.doubleValue, rhs.doubleValue)).decimalValue];
        };
    });
    return exponentOperator;
}

+(CALCOperator*) tenToTheOperator { //Returns the singleton instance of the x10^ operator.
    static CALCOperator *tenToTheOperator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tenToTheOperator = [[CALCOperator alloc] init];
        tenToTheOperator.precedence = CALCOperatorPrecedenceExponents;
        tenToTheOperator.stringValue = @"E";
        tenToTheOperator.evaluateExpression = ^ NSDecimalNumber* (NSDecimalNumber* lhs, NSDecimalNumber* rhs){
            if (rhs.integerValue > 0 && rhs.shortValue == rhs.doubleValue) {
                return [lhs decimalNumberByMultiplyingByPowerOf10:rhs.shortValue];
            }
            NSDecimalNumber *exponent = [NSDecimalNumber decimalNumberWithDecimal:@(pow(10.0, rhs.doubleValue)).decimalValue];
            return [lhs decimalNumberByMultiplyingBy:exponent];
        };
    });
    return tenToTheOperator;
}

-(NSDecimalNumber*) evaluateLeftHandSide:(NSDecimalNumber *)lhs andRightHandSide:(NSDecimalNumber *)rhs { //A convenience method to access the internal block for evaluating a left-hand side and a right-hand side value. Returns a decimal number.
    return self->_evaluateExpression(lhs, rhs);
}

@end
