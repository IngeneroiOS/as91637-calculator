//
//  CALCButton.h
//  Calculator
//
//  Created by Thomas Roughton on 4/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//
// Adapted from a tutorial by Ray Wenderlich, as found on http://www.raywenderlich.com/2134/core-graphics-101-glossy-buttons
//

#import <Foundation/Foundation.h>

void drawLinearGradient(CGContextRef context, CGRect rect, UIColor* startColour, UIColor*  endColour);
CGRect rectFor1PxStroke(CGRect rect);
void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef colour);
void drawGlossAndGradient(CGContextRef context, CGRect rect, UIColor* startColour, UIColor* endColour);
static inline double radians (double degrees) { return degrees * M_PI/180; }
CGMutablePathRef createArcPathFromBottomOfRect(CGRect rect, CGFloat arcHeight);
CGMutablePathRef createRoundedRectForRect(CGRect rect, CGFloat radius);