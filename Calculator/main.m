//
//  main.m
//  Calculator
//
//  Created by Thomas Roughton on 1/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CALCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CALCAppDelegate class]));
    }
}

/*  A (somewhat) brief overview of Objective-C syntax:
    Objective-C is a strict superset of C, so all C syntax is valid. Objective-C adds only one new data type: the object. All other code is C compliant.
    Instance methods are written in the following format:
 
-(return type) methodName:(Type of first parameter)firstParameter andSecondParameter:(Type of second parameter)secondParameter;
 
    A static or class method is written in much the same way, only with a + instead of a - at the start. C functions are also valid inside or outside of classes, and can return Obj-C types.
 
    To call the above method, you would write this:
    
    [instance methodName:firstParameter andSecondParameter:secondParameter];

    This, rather than calling a method, sends a message to that object, which may or not be able to respond. This dynamism is what allows Objective-C's undefined object type, id. All method calls are only evaluated at runtime, with the receiving object throwing an exception if it can't respond to the message (called a SEL, selector).

    Objective-C makes rather liberal use of pointers by having every object be referred to by its pointer. This is a requirement of the runtime. In practice, this simply means that instead of writing this:

    Class instanceName = [[Class alloc] init]; // (or [Class new], which is simply shorthand for alloc init)

    you write this:

    Class *instanceName = [[Class alloc] init];

    Class declarations must be done in two parts. The first, the interface, is declared in the format (usually in a header file, .h):
 
@interface MyClass : Superclass {
    instance variables
} 
 
method prototypes
 
properties (instance variables with auto-generated getters and setters)
 
@end

    This can be extended in the corresponding implementation file (extension .m), ommiting the superclass declaration.
 
 @interface MyClass () {
    private instance variables
 }
 etc.
 @end
 
    Then comes the class implementation.
 
 @implementation MyClass
 
 methods
 
 @end
 
    Objective-C has its own string class, NSString, for which literals are defined with an @ symbol before them e.g. @"MyString". It also has NS(Mutable)Array's, roughly equivalent to Java's ArrayLists, however the syntax for accessing and setting elements is no more than shorthand to its methods, and the arrays are not true literals.
 
 NSArray *array = @[objectOne, objectTwo]; //Only evaluated at runtime, so must be within method scope. Equivalent to assigning the result of the method call +[NSArray arrayWithObjects:objectOne, objectTwo, nil] (which returns a NSArray*)
 id object = array[0] //Equivalent to [array objectAtIndex:0]
 
    Finally, Objective-C has manual memory management, keeping count of the number of -[ retain] and -[ release] messages an object is sent and dealloc'ing (freeing) the object when the retainCount == 0. However, recently compilers have gained Automatic Reference Counting, wherein the compiler inserts these calls optimally instead of the developer. I have chosen to use ARC for this project to simplify development.
 
 */
