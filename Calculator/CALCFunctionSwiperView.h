//
//  CALCFunctionSwiperView.h
//  Calculator
//
//  Created by Thomas Roughton on 15/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CALCFunctionSwiperView : UIView

@property (nonatomic, strong) NSArray *buttons;

@end
