//
//  CALCOperator+BasicArithmetic.h
//  Calculator
//
//  Created by Thomas Roughton on 2/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import "CALCOperator.h"

@interface CALCOperator (BasicArithmetic)

+(CALCOperator*) additionOperator;
+(CALCOperator*) subtractionOperator;
+(CALCOperator*) multiplicationOperator;
+(CALCOperator*) divisionOperator;

@end
