//
//  CalculatorTests.m
//  CalculatorTests
//
//  Created by Thomas Roughton on 1/03/13.
//  Copyright (c) 2013 Ingenero Software. All rights reserved.
//

#import "CalculatorTests.h"

@implementation CalculatorTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in CalculatorTests");
}

@end
